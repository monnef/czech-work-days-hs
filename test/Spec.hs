module Main where

import           CzechWorkDays

import           Control.Monad
import           Data.Function    ((&))
import           Data.Maybe       (isJust, isNothing)
import           Data.Time        (fromGregorian)
import           System.Exit      (exitFailure)
import           Test.Tasty
import           Test.Tasty.HUnit

-- year, month, day
fg = fromGregorian

testYearlyHoliday msg year month day = do
  let cue = msg ++ " - " ++ show day ++ ". " ++ show month ++ ". " ++ show year
  assertEqual cue False $ isWorkDay $ fg year month day

years = [1950 .. 2050]

unitTests =
  testGroup
    "Unit tests"
    [ testCase "New Year 2000" $ isWorkDay (fg 2000 1 1) @?= False
    , testCase "21. 7. 2018 saturday" $ isWorkDay (fg 2018 7 21) @?= False
    , testCase "20. 7. 2018 normal friday" $ isWorkDay (fg 2018 7 20) @?= True
    , testCase "Yearly holidays" $ do
        let createTest (name, month, day) year = testYearlyHoliday name year month day
        let yearlyHolidays =
              map
                createTest
                [ ("New Year", 1, 1)
                , ("International Workers' Day", 5, 1)
                , ("Victory Day", 5, 8)
                , ("Den slovanských věrozvěstů Cyrila a Metoděje", 7, 5)
                , ("Den upálení mistra Jana Husa", 7, 5)
                , ("Den české státnosti", 9, 28)
                , ("Den vzniku samostatného československého státu", 10, 28)
                , ("Den boje za svobodu a demokracii", 11, 17)
                , ("Christmas Eve", 12, 24)
                , ("Nativity of Jesus", 12, 25)
                , ("Saint Stephen's Day", 12, 26)
                ]
        mapM_ (`mapM_` years) yearlyHolidays
    , testCase "Good Friday 2018" $ do
        let d = fg 2018 3 30
        isWorkDay d @?= False
        isJust (getEasterFriday d) @?= True
    , testCase "Easter Monday 2018" $ do
        let d = fg 2018 4 2
        isWorkDay d @?= False
        isJust (getEasterMonday d) @?= True
    , testCase "Good Friday 2001" $ isWorkDay (fg 2001 4 13) @?= False
    , testCase "Easter Monday 2001" $ isWorkDay (fg 2001 4 16) @?= False
    , testCase "Readme example" $ do
        let d = fromGregorian 2018 1 1
        isWorkDay d @?= False
        getHoliday d @?= Just "Nový rok"
        countOfWorkdaysInMonth 2018 7 @?= 20
    , testCase "getHoliday" $ do
        isJust (getHoliday (fg 1999 1 1)) @?= True
        isNothing (getHoliday (fg 2018 7 20)) @?= True
    , testCase "countOfHolidaysInMonth" $ countOfHolidaysInMonth 2018 7 @?= 2
    , testCase "countOfWorkdaysInMonth" $ countOfWorkdaysInMonth 2018 7 @?= 20
    , testCase "getHolidaysInMonth" $ do
        let exp =
              [ (fromGregorian 2018 7 5, "Den slovanských věrozvěstů Cyrila a Metoděje")
              , (fromGregorian 2018 7 6, "Den upálení mistra Jana Husa")
              ]
        getHolidaysInMonth 2018 7 @?= exp
    , testCase "getNonWeekendHolidaysInMonth" $ do
        let (y, m) = (2017, 10)
        getHolidaysInMonth y m @?= [(fromGregorian y m 28, "Den vzniku samostatného československého státu")]
        getNonWeekendHolidaysInMonth y m @?= []
    , testCase "countOfNonWeekendHolidaysInMonth" $ do
        countOfNonWeekendHolidaysInMonth 2017 10 @?= 0
        countOfNonWeekendHolidaysInMonth 2018 7 @?= 2
    ]

tests = testGroup "Tests" [unitTests]

main = defaultMain tests
