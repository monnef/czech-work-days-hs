{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE TypeSynonymInstances   #-}

module CzechWorkDays
  ( isWorkDay
  , isWeekend
  , isEasterHoliday
  , getFixedHoliday
  , getEasterMonday
  , getEasterFriday
  , getHoliday
  , countOfHolidaysInMonth
  , countOfWorkdaysInMonth
  , getHolidaysInMonth
  , getWorkdaysInMonth
  , getNonWeekendHolidaysInMonth
  , countOfNonWeekendHolidaysInMonth
  ) where

import           Control.Arrow               ((>>>))
import           Control.Lens                (makeFields, (^.))
import           Control.Lens.Operators      ((<&>))
import           Data.Function               ((&))
import           Data.List                   (find)
import           Data.Maybe                  (catMaybes, isJust)
import           Data.Monoid                 ((<>))
import           Data.Time                   (Day, fromGregorian, toGregorian)
import           Data.Time.Calendar          (addDays, gregorianMonthLength)
import           Data.Time.Calendar.Easter   (gregorianEaster)
import           Data.Time.Calendar.WeekDate (toWeekDate)
import           Data.Tuple.Utils

data FixedDateHoliday =
  FixedDateHoliday
    { _fixedDateHolidayMonth :: Int
    , _fixedDateHolidayDay   :: Int
    , _fixedDateHolidayName  :: String
    }

makeFields ''FixedDateHoliday

fixedHolidays =
  [ FixedDateHoliday 1 1 "Nový rok"
  , FixedDateHoliday 5 1 "Svátek práce"
  , FixedDateHoliday 5 8 "Den vítězství"
  , FixedDateHoliday 7 5 "Den slovanských věrozvěstů Cyrila a Metoděje"
  , FixedDateHoliday 7 6 "Den upálení mistra Jana Husa"
  , FixedDateHoliday 9 28 "Den české státnosti"
  , FixedDateHoliday 10 28 "Den vzniku samostatného československého státu"
  , FixedDateHoliday 11 17 "Den boje za svobodu a demokracii"
  , FixedDateHoliday 12 24 "Štědrý den"
  , FixedDateHoliday 12 25 "1. svátek vánoční"
  , FixedDateHoliday 12 26 "2. svátek vánoční"
  ]

getFixedHolidayInt :: Int -> Int -> Maybe String
getFixedHolidayInt tMonth tDay = find matches fixedHolidays <&> (^. name)
  where
    matches :: FixedDateHoliday -> Bool
    matches fh = fh ^. month == tMonth && fh ^. day == tDay

getFixedHoliday :: Day -> Maybe String
getFixedHoliday d = getFixedHolidayInt (getGregorianMonth d) (getGregorianDay d)

isFixedHoliday :: Day -> Bool
isFixedHoliday d = isJust $ getFixedHoliday d

isEasterHoliday :: Day -> Bool
isEasterHoliday d = any isJust [getEasterMonday d, getEasterFriday d]

getEasterMonday :: Day -> Maybe String
getEasterMonday d =
  if d == monday
    then Just "Velikonoční pondělí"
    else Nothing
  where
    easter = gregorianEaster $ getGregorianYear d
    monday = addDays 1 easter

getEasterFriday :: Day -> Maybe String
getEasterFriday d =
  if d == friday
    then Just "Velký pátek"
    else Nothing
  where
    easter = gregorianEaster $ getGregorianYear d
    friday = addDays (-2) easter

getGregorianYear :: Day -> Integer
getGregorianYear = fst3 . toGregorian

getGregorianMonth :: Day -> Int
getGregorianMonth = snd3 . toGregorian

getGregorianDay :: Day -> Int
getGregorianDay = thd3 . toGregorian

getWeekend :: Day -> Maybe String
getWeekend d =
  case weekDayNum d of
    6 -> Just "Sobota"
    7 -> Just "Neděle"
    _ -> Nothing

weekDayNum :: Day -> Int
weekDayNum d = thd3 $ toWeekDate d

isWeekend :: Day -> Bool
isWeekend d = isJust $ getWeekend d

isWorkDay :: Day -> Bool
isWorkDay = isNotWorkDay >>> not

isNotWorkDay :: Day -> Bool
isNotWorkDay d = any ($ d) [isWeekend, isFixedHoliday, isEasterHoliday]

getHoliday :: Day -> Maybe String
getHoliday d = mconcat $ map ($ d) [getFixedHoliday, getEasterMonday]

isHoliday :: Day -> Bool
isHoliday d = isJust $ getHoliday d

getDaysInMonth :: Integer -> Int -> [Day]
getDaysInMonth tYear tMonth = allDays
  where
    allDaysCount = gregorianMonthLength tYear tMonth
    allDays = map (fromGregorian tYear tMonth) [1 .. allDaysCount]

getHolidaysInMonth :: Integer -> Int -> [(Day, String)]
getHolidaysInMonth tYear tMonth = allDays & toMaybeHolidays <&> convert & catMaybes
  where
    allDays :: [Day]
    allDays = getDaysInMonth tYear tMonth
    toMaybeHolidays :: [Day] -> [(Day, Maybe String)]
    toMaybeHolidays = map (\d -> (d, getHoliday d))
    convert (d, Just s)  = Just (d, s)
    convert (d, Nothing) = Nothing

countOfHolidaysInMonth :: Integer -> Int -> Int
countOfHolidaysInMonth tYear tMonth = length $ getHolidaysInMonth tYear tMonth

getWorkdaysInMonth :: Integer -> Int -> [Day]
getWorkdaysInMonth tYear tMonth = filter isWorkDay (getDaysInMonth tYear tMonth)

countOfWorkdaysInMonth :: Integer -> Int -> Int
countOfWorkdaysInMonth tYear tMonth = length $ getWorkdaysInMonth tYear tMonth

getNonWeekendHolidaysInMonth :: Integer -> Int -> [(Day, String)]
getNonWeekendHolidaysInMonth tYear tMonth = getHolidaysInMonth tYear tMonth & filter (fst >>> isWeekend >>> not)

countOfNonWeekendHolidaysInMonth :: Integer -> Int -> Int
countOfNonWeekendHolidaysInMonth tYear tMonth = length $ getNonWeekendHolidaysInMonth tYear tMonth
