# czech-work-days-hs

Simple CLI tool which computes number of work days in a given month of a specific year.

Ported from Eta which turned out to not be very mature yet (tooling, breaking changes). Also JVM is not well suited for short lived console programs - Haskell version is more than 40 times faster and consumed less than 30 times of memory.

# Example

```sh
$ czech-work-days-hs 2019 12
19
```

# Requirements

* [stack](http://haskellstack.org)

# Installation

```sh
stack install
```

# Tests

```sh
stack test
```

# License
**GPL3**
