module Main where

import           CzechWorkDays          (countOfWorkdaysInMonth)

import           Control.Lens.Operators ((<&>))
import           Data.Monoid
import           Safe                   (readEitherSafe)
import           System.Environment
import           System.Exit

readIntOrDie :: String -> String -> IO Int
readIntOrDie msg x = do
  let res = readEitherSafe x :: Either String Int
  case res of
    Left err -> die $ msg <> err
    Right r  -> return r

main :: IO ()
main = do
  args <- getArgs
  case args of
    [yearStr, monthStr] -> do
      year <- readIntOrDie "Failed to parse year: " yearStr <&> fromIntegral
      month <- readIntOrDie "Failed to parse month: " monthStr
      print $ countOfWorkdaysInMonth year month
    _ -> die "Wrong number of arguments. Expecting: YEAR MONTH_NUM"
